# simple forward Euler solver for the 1D heat equation
#   u_t = kappa u_xx  0<x<L, 0<t<T
# with zero-temperature boundary conditions
#   u=0 at x=0,L, t>0
# and prescribed initial temperature
#   u=u_I(x) 0<=x<=L,t=0

import numpy as np
import pylab as pl
from math import pi
import scipy.sparse as sp
from scipy.sparse import linalg


# set problem parameters/functions
kappa = 1.0  # diffusion constant
L=1.0        # length of spatial domain
T=0.5        # total time to solve for
def u_I(x):
    # initial temperature distribution
    y = np.sin(pi*x/L)
    return y

def u_exact(x,t):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y
	
def u_0(t):
	return 0
	
def u_L(t):
	return 0

# set numerical parameters
mx = 10   # number of gridpoints in space
mt = 1000   # number of gridpoints in time


# set up the numerical environment variables
x = np.linspace(0, L, mx+1)     	# mesh points in space
t = np.linspace(0, T, mt+1)     	# mesh points in time
deltax = x[1] - x[0]            	# gridspacing in x
deltat = t[1] - t[0]            	# gridspacing in t
lmbda = kappa*deltat/(deltax**2)    # mesh fourier number
print("deltax=",deltax)
print("deltat=",deltat)
print("lambda=",lmbda)

# set up the solution variables
u_j = np.zeros(x.size)        # u at current time step
u_jp1 = np.zeros(x.size)      # u at next time step

# Set initial condition
u_j = u_I(x)

# Create diagonals for difference matrix
diags = np.ones(mx)
udiag = -diags*lmbda
centre = 1 + 2*lmbda*np.ones(mx+1)
ldiag = -diags*lmbda 

udiag[0] = 2*udiag[0]
ldiag[-1] = 2*ldiag[-1]




# Create backwards difference matrix
A = sp.diags([ldiag,centre,udiag],[-1,0,1], shape=(mx+1,mx+1)) #Not including boundary conditions
bounds = np.zeros(mx+1)

# Solve the PDE: loop over all time points
for n in range(1, mt+1):
		bounds[0] = -u_0(n); bounds[-1] = u_L(n)
		u_jp1 = linalg.spsolve(A,u_j + 2*lmbda*deltax*bounds) 

		
	
		# Boundary conditions
		#u_jp1[0] = -u_0(n); u_jp1[-1] = u_L(n);
		
		# Update u_j
		u_j[:] = u_jp1[:]

# plot the final result and exact solution
pl.plot(x,u_j,'ro',label='num')
xx = np.linspace(0,L,250)
#pl.plot(xx,u_exact(xx,T),'b-',label='exact')
pl.xlabel('x')
pl.ylabel('u(x,0.5)')
pl.legend(loc='upper right')
pl.show()