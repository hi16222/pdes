from numpy import *
import pylab as pl
import matplotlib.pyplot as plt
import scipy.sparse as sp
from scipy.sparse import linalg


def u_I(x,L):
	# initial temperature distribution
	y = sin(pi*x/L)
	return y

def u_exact(x,t,kappa,L):
	# the exact solution
	y = exp(-kappa*(pi**2/L**2)*t)*sin(pi*x/L)
	return y
	
def u_0(t):
	# boundary condition
	return 0
	
def u_L(t):
	# boundary condition 
	return 0

def F(t):
	# additional heat source
	return 0


def Forwards(u_j,u_jp1,lmbda,deltat,deltax,mx,mt,bounds,shape,lowerbound,upperbound,lb,ub):
	# Create forwards euler matrix
	A = create_matrix(lmbda, 'Forwards', shape,lowerbound,upperbound)

	# Solve the PDE: loop over all time points
	for n in range(1, mt+1):
		# Forward Euler timestep at inner mesh points
		
		bounds[0] = -2*deltax*u_0(n); bounds[-1] = 2*deltax*u_L(n)
		u_jp1[lb:ub] = A.dot(u_j[lb:ub])+ lmbda*bounds + deltat * F(u_j)
		
		if lowerbound == 'Dirichlet':
			u_jp1[0] = u_0(n); 
		if upperbound == 'Dirichlet':
			u_jp1[-1] = u_L(n);
		
		# Update u_j
		u_j[:] = u_jp1[:]
	
	return u_j 

def Backwards(u_j,u_jp1,lmbda,deltat,deltax,mx,mt,bounds,shape,lowerbound,upperbound,lb,ub):
	A = create_matrix(lmbda, 'Backwards', shape,lowerbound,upperbound) 
	
	
	# Solve the PDE: loop over all time points
	for n in range(1, mt+1):
		# backwards Euler timestep at inner mesh points
		
		bounds[0] = -2*deltax*u_0(n); bounds[-1] = -2*deltax*u_L(n)
		u_jp1[lb:ub] = linalg.spsolve(A,u_j[lb:ub] + lmbda*bounds + deltat * F(u_j)) 

		
	
		# Boundary conditions
		if lowerbound == 'Dirichlet':
			u_jp1[0] = u_0(n); 
		if upperbound == 'Dirichlet':
			u_jp1[-1] = u_L(n);
		
		
		# Update u_j
		u_j[:] = u_jp1[:]
		
	return u_j	

def Crank_Nicholson(u_j,u_jp1,lmbda,deltat,deltax,mx,mt,bounds,shape,lowerbound,upperbound,lb,ub):
	# Create backwards difference matrix
	A,B = create_matrix(lmbda,'Crank_Nicholson',shape,lowerbound,upperbound)

	# Solve the PDE: loop over all time points
	for n in range(1, mt+1):
		# Central Euler timestep at inner mesh points
		
		bounds[0] =  -2*deltax*u_0(n); bounds[-1] =  -2*deltax*u_L(n)
		u_jp1[lb:ub] = linalg.spsolve(A,B.dot(u_j[lb:ub]) + lmbda*bounds + deltat * F(u_j)) 
		
		# Boundary conditions
		if lowerbound == 'Dirichlet':
			u_jp1[0] = u_0(n); 
		if upperbound == 'Dirichlet':
			u_jp1[-1] = u_L(n);
			
		# Update u_j
		u_j[:] = u_jp1[:]
	
	return u_j
	
	
def create_matrix(lmbda,difference,shape,lowerbound,upperbound):

	# Set diagonals
	diags = ones(shape-1)
	udiag = diags*lmbda
	ldiag = diags*lmbda
	
	# Set 2*lambda if Neumann boundaries 
	if lowerbound == 'Neumann':
		ldiag[-1] = 2*diags[-1]*lmbda
	if upperbound == 'Neumann':
		udiag[0] = 2*diags[0]*lmbda
	
	if difference == 'Forwards': 
		return sp.diags([ldiag,1 - 2*lmbda,udiag],[-1,0,1], shape=(shape,shape))
	elif difference == 'Backwards': 
		return sp.diags([-ldiag,1 + 2*lmbda,-udiag],[-1,0,1], shape=(shape,shape))
	elif difference == 'Crank_Nicholson':
		A = sp.diags([-ldiag/2,1+lmbda,-udiag/2],[-1,0,1], shape=(shape,shape))
		B = sp.diags([ldiag/2,1-lmbda,udiag/2],[-1,0,1], shape=(shape,shape))
		return  A,B
	else:
		return -1;
	
	

	
def main(xmax,tmax,xsteps,tsteps,lowerbound = 'Dirichlet',upperbound = 'Dirichlet',	difference = Forwards, plot = False):

	# set problem parameters/functions
	kappa = 1.0    # diffusion constant
	L= xmax        # length of spatial domain
	T= tmax        # total time to solve for
		
	# set numerical parameters
	mx = xsteps    # number of gridpoints in space
	mt = tsteps    # number of gridpoints in time

	# set up the numerical environment variables
	x = linspace(0, L, mx+1)     # mesh points in space
	t = linspace(0, T, mt+1)     # mesh points in time
	deltax = x[1] - x[0]            # gridspacing in x
	deltat = t[1] - t[0]            # gridspacing in t
	lmbda = kappa*deltat/(deltax**2)    # mesh fourier number
	print("deltax=",deltax)
	print("deltat=",deltat)
	print("lambda=",lmbda)

	# set up the solution variables
	u_j = zeros(x.size)        # u at current time step
	u_jp1 = zeros(x.size)      # u at next time step

	# Set initial condition
	u_j = u_I(x,L)
	
	# initial shapes for Dirichlet
	shape = mx-1 
	lb = 1
	ub = -1
	
	# Change shapes if Neumann
	if lowerbound == 'Neumann':
		shape = shape + 1
		lb = 0
	
	if upperbound == 'Neumann':
		shape = shape + 1
		ub = shape + 1
	
	# initialise boundary conditions
	boundary_conds = zeros(shape)
	
	# Compute finite difference
	y = difference(u_j,u_jp1,lmbda,deltat,deltax,mx,mt,boundary_conds,shape,lowerbound,upperbound,lb,ub)
	
	# Error
	sqDiff = (abs(u_exact(x,T,kappa,L) - y))**2
	Error = (L*sqDiff)**0.5
	
	# if True plot the solution
	if plot == True:
		pl.plot(x,y,'ro',label='num')
		xx = linspace(0,L,250)
		#pl.plot(xx,u_exact(xx,T,kappa,L),'b-',label='exact')
		pl.xlabel('x')
		pl.ylabel('u(x,0.5)')
		pl.title(difference.__name__)
		pl.legend(loc='upper right')
		pl.show()
	
	return Error[1],deltax
	
if __name__ == '__main__':
	
	# Initialisation
	deltax = zeros(10)
	deltat = zeros(10)
	Errorx = zeros(10)
	Errorx2 = zeros(10)
	Errorx3 = zeros(10)
	
	# calculates error at different values of deltax
	for i in range(10):
		Errorx[i], deltax[i] = main(1,0.5,20*(i+1),10000) 
		Errorx2[i], deltax[i] = main(1,0.5,20*(i+1),10000,difference = Backwards)
		Errorx3[i], deltax[i]= main(1,0.5,20(i+1),10000,difference = Crank_Nicholson)
	
	# Plot the logs of error and deltax
	plt.loglog(Errorx,deltax,'-x')
	plt.loglog(Errorx2,deltax,'-x')
	plt.loglog(Errorx3,deltax,'-x')
	plt.legend(['F','B','CN'])
	plt.show()
	