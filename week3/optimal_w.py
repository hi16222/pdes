import numpy as np
from Successive_Over_Relaxation import SOR

start = 1
end = 2
step = 0.01

omega = np.arange(start,end,step)
counts = np.zeros_like(omega)

for i in range(len(omega)):
	counts[i] = SOR(omega[i],2,1,40,20,10e-4,1000)
	
#optimal = round(omega[min(counts)==counts][0],2)

print('The optimal value of omega is ',omega[min(counts)==counts][0])
print('Iterations =', min(counts))
