
import numpy as np
import pylab as pl
from math import pi
import scipy.sparse as sp
from scipy.sparse import linalg

# Set problem parameters/functions
c=1.0         # wavespeed
L=1.0         # length of spatial domain
T=2.0         # total time to solve for
alpha = 1
sigma = 1

def u_I(x):
    # initial displacement
    y = h(x) + alpha*np.exp(-(x-x)**2/sigma**2)
    return y

def h(x):
	# height above seabed
	y = 10
	return y
	
def v_I(x):
    # initial velocity
    y = np.zeros(x.size)
    return y

	
def u_0(t):
	# left bound
	return 0

def u_L(t):
	# right bound
	return 0

def u_exact(x,t):
    # the exact solution
    y = np.cos(x)
    return y

# Set numerical parameters
mx = 40    # number of gridpoints in space
mt = 500     # number of gridpoints in time


# Set up the numerical environment variables
x = np.linspace(0, L, mx+1)     # gridpoints in space
t = np.linspace(0, T, mt+1)     # gridpoints in time
deltax = x[1] - x[0]            # gridspacing in x
deltat = t[1] - t[0]            # gridspacing in t
lmbda = c*deltat/deltax         # squared courant number
print("lambda=",lmbda)

# set up the solution variables
u_jm1 = np.zeros(x.size)        # u at previous time step
u_j = np.zeros(x.size)          # u at current time step
u_jp1 = np.zeros(x.size)        # u at next time step

# Set initial condition
u_jm1 = u_I(x)    

# matrix set up
diags = np.ones(mx)
udiag = diags*(0.5*lmbda**2)
ldiag = diags*(0.5*lmbda**2)

# First timestep boundary condition
bounds = np.zeros(mx-1)


# initialise matrices
A = sp.diags([-ldiag, 1 + lmbda**2,-udiag],[-1,0,1],shape=(mx-1,mx-1))
B = sp.diags([ldiag,-1 - lmbda**2,udiag],[-1,0,1],shape=(mx-1,mx-1))


 
# First time step 
u_j[1:-1] = A.dot(u_I(x[1:-1])) + deltat*v_I(x[1:-1]) 
u_j[0] = h(x); u_j[-1] = h(x);



# Solve the PDE: loop over all time points
for n in range(2, mt+1):
    # regular timestep at inner mesh points
    bounds[0] = u_0(n); bounds[-1] = u_L(n)
    u_jp1[1:-1] = linalg.spsolve(A,(2*(u_j[1:-1]) + B.dot(u_jm1[1:-1])+ lmbda*bounds))
  
    # boundary conditions
    u_jp1[0] = u_j[0] - lmbda*(u_j[0] - u_jm1[0]); u_jp1[-1] = u_j[-1] - lmbda*(u_j[-1] - u_jm1[-1])
            
    # update u_jm1 and u_j
    u_jm1[:],u_j[:] = u_j[:],u_jp1[:]

# Plot the final result and exact solution
pl.plot(x,u_jp1,'-ro',label='numeric')
xx = np.linspace(0,L,250)
pl.plot(xx,u_exact(xx,T),'b-',label='exact')
pl.xlabel('x')
pl.ylabel('u(x,T)')
pl.legend()
pl.show()