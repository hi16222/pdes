# simple explicit finite difference solver for the 1D wave equation
#   u_tt = c^2 u_xx  0<x<L, 0<t<T
# with zero-displacement boundary conditions
#   u=0 at x=0,L, t>0
# and prescribed initial displacement and velocity
#   u=u_I(x), u_t=v_I(x)  0<=x<=L,t=0

import numpy as np
import pylab as pl
from math import pi
import scipy.sparse as sp

# Set problem parameters/functions
c=1.0         # wavespeed
L=1.0         # length of spatial domain
T=2.0         # total time to solve for

# Set numerical parameters
mx = 40      # number of gridpoints in space
mt = 80      # number of gridpoints in time


def u_I(x):
    # initial displacement
    y = np.sin(pi*x/L)
    return y

def v_I(x):
    # initial velocity
    y = np.zeros(x.size)
    return y

def u_0(t):
	return 0

def u_L(t):
	return 0

def u_exact(x,t):
    # the exact solution
    y = np.cos(pi*c*t/L)*np.sin(pi*x/L)
    return y




# Set up the numerical environment variables
x = np.linspace(0, L, mx+1)     # gridpoints in space
t = np.linspace(0, T, mt+1)     # gridpoints in time
deltax = x[1] - x[0]            # gridspacing in x
deltat = t[1] - t[0]            # gridspacing in t
lmbda = c*deltat/deltax         # squared courant number
print("lambda=",lmbda)

# set up the solution variables
u_jm1 = np.zeros(x.size)        # u at previous time step
u_j = np.zeros(x.size)          # u at current time step
u_jp1 = np.zeros(x.size)        # u at next time step

# Set initial condition
u_jm1 = u_I(x)    

diags = np.ones(mx)
udiag = diags*(lmbda**2)
centre = 2 - 2*(lmbda**2)*np.ones(mx+1)
ldiag = diags*(lmbda**2)
ldiag[-1] = ldiag[-1]*2
udiag[0] = udiag[0]*2 


# First timestep boundary condition
bounds = np.zeros(mx+1)


A = sp.diags([ldiag,centre,udiag],[-1,0,1],shape=(mx+1,mx+1))
 
# First time step 
bounds[0] = -2*deltax*u_0(0); bounds[-1] = 2*deltax*u_L(0);
u_j = 0.5*A.dot(u_jm1) + deltat*v_I(x) +lmbda*bounds
#u_j[0] = u_0(0); u_j[-1]=u_L(0);

'''
For the tsunami set u_j as h(0)
'''



# Solve the PDE: loop over all time points
for n in range(2, mt+1):
    # regular timestep at inner mesh points
    bounds[0] = -2*deltax*u_0(n); bounds[-1] = 2*deltax*u_L(n);
    u_jp1 = A.dot(u_j)- u_jm1 + lmbda*bounds
  
    # boundary conditions
    #u_jp1[0] = u_0(n); u_jp1[-1] = u_L(n)
            
    # update u_jm1 and u_j
    u_jm1[:],u_j[:] = u_j[:],u_jp1[:]

# Plot the final result and exact solution
pl.plot(x,u_jp1,'-ro',label='numeric')
xx = np.linspace(0,L,250)
#pl.plot(xx,u_exact(xx,T),'b-',label='exact')
pl.xlabel('x')
pl.ylabel('u(x,T)')
pl.legend()
pl.show()